import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { } from 'googlemaps';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewInit {
  @ViewChild('googleMap', { static: false }) gmapElement: ElementRef;
  map: google.maps.Map;

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    this.loadMap();
  }

  loadMap() {
    const mapProp = {
      center: new google.maps.LatLng(12.9716, 77.5946),
      zoom: 15,
      disableDefaultUI: true
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    const centerControlDiv = document.createElement('div');
    this.CenterControl(centerControlDiv, this.map);
    this.map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(centerControlDiv);
  }

  CenterControl(controlDiv, map) {

    // Set CSS for the control.
    const controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = 'solid 2px #5e6595';
    controlUI.style.borderRadius = '3px';
    controlUI.style.cursor = 'pointer';
    controlUI.style.textAlign = 'center';
    controlUI.style.height = '40px';
    controlUI.style.width = '220px';
    controlUI.style.marginRight = '30px';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    const controlText = document.createElement('div');
    controlText.style.color = '#44496b';
    controlText.style.fontFamily = 'Roboto-Medium';
    controlText.style.fontSize = '17px';
    controlText.style.padding = '8px';
    controlText.innerHTML = 'Get Direction' + '<img style="margin-left: 15px" src="../../../assets/images/shape_3.png"/>';
    controlUI.appendChild(controlText);
  }


}
