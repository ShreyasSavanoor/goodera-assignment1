import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  footerlists = [
    {
      image: true,
      imgSrc: 'download-21',
    },
    {
      image: false,
      label: 'Announcements'
    },
    {
      count: true,
      number: '2'
    },
    {
      image: true,
      imgSrc: 'chevron-up'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
