import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  @Input() images;
  progress = [
    {
      progress: '100%',
      listStyle: {
        'margin-left': '25px',
        width: 'calc((100% / 5) - 25px)'
      },
      progressValue: {
        width: '100%'
      }
    },
    {
      progress: '25%',
      listStyle: {
        width: 'calc(100% / 5)'
      },
      progressValue: {
        width: '50%'
      }
    },
    {
      progress: '0',
      listStyle: {
        width: 'calc(100% / 5)'
      },
      progressValue: {
        width: '0%'
      }
    },
    {
      progress: '0',
      listStyle: {
        width: 'calc(100% / 5)'
      },
      progressValue: {
        width: '0%'
      }
    },
    {
      progress: '0',
      listStyle: {
        'margin-right': '15px',
        width: 'calc((100% / 5) - 15px)'
      },
      progressValue: {
        width: '0%'
      }
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
