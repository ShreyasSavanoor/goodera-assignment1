import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  navigationsLeft = [
    {
      label: 'Goodera',
      labelStyle: {
        'font-size': '25px',
        color: '#ff7a49'
      }
    },
    {
      label: 'CAMPAIGNS'
    },
    {
      label: 'VOLUNTEER'
    },
    {
      label: 'DONATE'
    },
    {
      label: 'STORIES'
    },
  ];
  navigationsRight = [
    {
      label: 'ADMIN',
      listStyle: {
        'border-right': '1px solid #dfdfdf'
      }
    },
    {
      img: true,
      imgSrc: 'group-6',
      listStyle: {
        'border-right': '1px solid #dfdfdf'
      }
    },
    {
      user: true,
      img: true,
      imgSrc: 'oval-5'
    },
    /* {
      img: true,
      imgSrc: 'add-1',
      listStyle: {
        background: '#44496b'
      }
    } */
  ];

  constructor() { }

  ngOnInit() {
  }

}
