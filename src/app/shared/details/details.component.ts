import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  info = [
    {
      label: 'Focus Area',
      value: 'Nutrition | Employability/Skills/Livelihood',
      listStyle: {
        width: 'calc((100% / 3) - 2px)',
        'border-right': 'solid 1px #eae8e8'
      },
      labelStyle: {
        display: 'block',
        'font-family': 'Roboto-Medium',
        'font-size': '14px',
        'font-weight': '500',
        color: '#202020'
      },
      valueStyle: {
        display: 'block',
        'font-family': 'Roboto-Regular',
        'font-size': '14px',
        color: '#001919'
      }
    },
    {
      label: 'Share & Invite',
      value: '',
      imageList: true,
      imageListFloat: 'right',
      imgSrc: ['group-8', 'group-26', 'group-49', 'group-21'],
      listStyle: {
        width: 'calc((100% / 3) - 2px)',
        'border-right': 'solid 1px #eae8e8',
        'text-align': 'center'
      },
      labelStyle: {
        position: 'relative',
        top: '-15px',
        'margin-right': '10px',
        'font-family': 'Roboto-Medium',
        'font-size': '14px',
        'font-weight': '500',
        color: '#202020'
      },
      imgStyle: {
        'margin-right': '5px'
      }
    },
    {
      peopleLists: true,
      label: '',
      value: '',
      peopleCount: '120',
      slotsLeft: '26',
      imageList: true,
      imageListFloat: 'left',
      imgSrc: ['oval-5', 'oval-5_2', 'oval-5', 'oval-5_2'],
      listStyle: {
        width: 'calc((100% / 3) - 2px)',
        'text-align': 'center'
      }
    }
  ];
  details = [
    {
      content: 'In operation since 1979, Chicxulub Food Bank is a fixture of market life and part of a community approach to ensure that all residents of Mexico have access to quality and nutritious food. It provides food to nearly 1000 people weekly and is totally dependent on its volunteers for this.',
      paraStyle: {
        'font-family': 'Roboto-Regular',
        'font-size': '16px',
        color: '#001919'
      },
      labelStyle: {
        'display': 'inline-block',
        'margin-top': '8px',
        'font-family': 'Roboto-Medium',
        'font-size': '16px',
        'font-weight': '500',
        color: '#5e6595'
      }
    }
  ];
  perks = [
    {
      imgSrc: 'group-32',
      label: 'Avail transfers',
      labelStyle: {
        padding: '5px 0 0 5px'
      }
    },
    {
      imgSrc: 'group-50',
      label: 'Indoors',
      labelStyle: {
        padding: '10px 0 0 5px'
      }
    },
    {
      imgSrc: 'group-51',
      label: 'Open to family and friends',
      labelStyle: {
        padding: '10px 0 0 5px'
      }
    },
    {
      imgSrc: 'group-52',
      label: 'Goodies',
      labelStyle: {
        padding: '10px 0 0 5px'
      }
    }
  ];
  questions = [
    {
      expand: true,
      label: 'Skills Required',
      value: 'Volunteering is for everyone, your intent and time is all that matters.',
      listStyle: {
        margin: '25px 0'
      }
    },
    {
      expand: true,
      label: 'Who will benefit from my contribution?',
      value: 'Socially Backward Group   I Children  I  Women',
      listStyle: {
        margin: '25px 0'
      }
    },
    {
      expand: false,
      label: 'Why should I contribute?',
      listStyle: {
        padding: '25px 0',
        'border-top': 'solid 1px rgba(234, 232, 232, 0.74)',
        'border-bottom': 'solid 1px rgba(234, 232, 232, 0.74)'
      }
    },
    {
      expand: false,
      label: 'How will it make a difference?',
      listStyle: {
        padding: '25px 0',
        'border-top': 'solid 1px rgba(234, 232, 232, 0.74)',
        'border-bottom': 'solid 1px rgba(234, 232, 232, 0.74)'
      }
    }
  ];
  contacts = [
    {
      label: 'Event Captain (SPOC)',
      name: 'Martin Robinson',
      captain: true,
      userImg: 'oval-5',
      backgroundImage: 'group-2',
      phoneImg: 'phone',
      mailImg: 'mail',
      spocImg: 'group-30'
    },
    {
      label: 'Organizer Primary Contact',
      name: 'Georgia Craig',
      userImg: 'oval-5_2',
      backgroundImage: 'group-2',
      phoneImg: 'phone',
      mailImg: 'mail'
    }
  ];
  goals = [
    {
      imgSrc: 'screen-shot-2018-07-27-at-4-20-38-pm'
    },
    {
      imgSrc: 'screen-shot-2018-07-27-at-4-20-52-pm'
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
