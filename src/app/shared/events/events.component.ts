import { Component, OnInit, Input } from '@angular/core';
import { Content } from '@angular/compiler/src/render3/r3_ast';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  @Input() content;
  events = [
    {
      label: 'Event Schedule',
      imgSrc: 'clipboard',
      data: [],
      active: true
    },
    {
      label: 'Pre-requisites',
      imgSrc: 'file-text',
      data: [],
      active: false
    },
    {
      label: "Do’s & Dont’s",
      imgSrc: 'check-square',
      data: [],
      active: false
    },
    {
      label: '-',
      imgSrc: 'clipboard',
      data: [],
      active: false
    }
  ];
  leftTimeline = [
    {
      value: '00:09 AM'
    },
    {
      value: '10:09 AM'
    },
    {
      value: '10:00 PM'
    },
    {
      value: '03:05 PM'
    }
  ];
  rightTimeline = [
    {
      time: '12:00 AM - 12:30 AM',
      event: 'Orientation',
      description: 'Jill, the volunteer coordinator at the food bank let the volunteers know about the food bank’s history, mission and other dos and don’ts.'
    },
    {
      time: '12:30 AM - 1:30 AM',
      event: 'Food Collection',
      description: 'Volunteers will go around Pike Place Market and collect donations from the shops at the market. They will bring back the stock to the food bank to give away to customers.'
    },
    {
      time: '1:30 AM - 2:40 AM',
      event: 'Serve Customers at the Food Bank ',
      description: 'Volunteers will stand behind the counters, serve the customers at the food bank and make sure everyone goes home happy!'
    },
    {
      time: '2:40 AM - 3:00 AM',
      event: 'Team Retrospection',
      description: 'Jill will organize a group huddle to go through the days activities and decide on the things that made people happy and the things which could be improved upon.'
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
