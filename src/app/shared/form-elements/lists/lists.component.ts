import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class ListsComponent implements OnInit {
  @Input() headers;
  @Input() inline;
  @Input() col3;
  @Input() col4;
  @Input() type;
  @Input() perks;
  @Input() footer;
  constructor() { }

  ngOnInit() {
  }

}
