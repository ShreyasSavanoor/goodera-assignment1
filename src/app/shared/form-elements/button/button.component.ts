import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() text: string;
  @Input() background: string;
  @Input() color: string;
  @Input() float: string;
  @Input() padding: string;
  @Input() margin: string;
  @Input() width: string;
  @Input() height: string;
  @Input() textAlign: string;
  @Input() borderRadius: string;
  @Input() fontSize: string;
  @Input() border: string;
 

  constructor() { }

  ngOnInit() {
  }

}
