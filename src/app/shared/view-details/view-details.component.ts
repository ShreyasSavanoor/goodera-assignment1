import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-view-details',
  templateUrl: './view-details.component.html',
  styleUrls: ['./view-details.component.scss']
})
export class ViewDetailsComponent implements OnInit {
  @Input() headers: Array<any> = [];
  @Input() columns;
  @Input() type;
  @Input() left;
  @Input() right;
  constructor() { }

  ngOnInit() {
  }

}
