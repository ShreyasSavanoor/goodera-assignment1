import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: ['./participants.component.scss']
})
export class ParticipantsComponent implements OnInit {
  participants: Array<any> = [
    {
      imgScr: 'oval-5_3',
      name: 'Hazel graze Lancaster',
      designation: 'Project manager',
      place: 'CSR Team, Origgio VA',
    },
    {
      imgScr: 'oval-5_4',
      name: 'Garrett Curtis',
      designation: '',
      place: 'Tech team, Origgio VA',
    },
    {
      imgScr: 'oval-5_7',
      name: 'belee_deen@genpact.com',
      designation: '',
      place: 'Design team, Origgio VA',
    },
    {
      imgScr: 'oval-5_5',
      name: 'Martha Lowe',
      designation: '',
      place: 'Origgio VA',
    },
    {
      imgScr: 'oval-5_6',
      name: 'NG 110',
      designation: 'HR Manger',
      place: 'Origgio VA',
    },
    {
      imgScr: 'oval-5_3',
      name: 'Martin Mendoza',
      designation: '',
      place: '',
    },
    {
      imgScr: 'oval-5_7',
      name: 'Perly Maany',
      designation: 'Tech head',
      place: 'Tech team, Origgio VA',
    },
    {
      imgScr: 'oval-5_8',
      name: 'Angel D maria',
      designation: 'Project Manager',
      place: 'Product team, Origgio VA',
    },
    {
      imgScr: 'oval-5_5',
      name: 'Alex Tod',
      designation: 'Project lead',
      place: 'Prdouct team, Origgio VA',
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
