import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-opportunites',
  templateUrl: './opportunites.component.html',
  styleUrls: ['./opportunites.component.scss']
})
export class OpportunitesComponent implements OnInit {
  opportunities: Array<any> = [
    {
      showImage: true,
      imgScr: 'screenshot-2019-02-15-at-3-52-24-pm',
      label: 'Warehouse helper,walk-in sorter and distributer',
      place: 'Watford,United Kingdom',
      date: 'Thursday, December 12, 2019',
      peopleCount: '20',
      expiryDate: 'Dec 6th'
    },
    {
      showImage: true,
      imgScr: 'screenshot-2019-02-15-at-4-03-32-pm',
      label: 'Exercise, play & clean your beach',
      place: 'Chicxulub puerto, Mexico',
      date: 'Tuesday, February 12, 2019',
      peopleCount: '20',
      expiryDate: 'Feb 6th'
    },
    {
      showImage: true,
      imgScr: 'screenshot-2019-02-15-at-4-08-51-pm',
      label: 'Be a food collection agent for a day',
      place: 'Chicxulub puerto, Mexico',
      date: 'Sunday, October 13, 2019',
      peopleCount: '20',
      expiryDate: 'Oct 6th'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
