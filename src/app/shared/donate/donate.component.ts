import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.scss']
})
export class DonateComponent implements OnInit {
  donate: Array<any> = [
    {
      imageOnly: true,
      imgScr: 'rectangle-23'
    },
    {
      textOnly: true,
      label: 'Donating goods to those in need',
      para: "Here's a glimpse into the lives of the kids in Sewri. Help them receive an excellent education. Here's a glimpse into the lives of the kids.",
      pledgeAmount: '$.28,304',
      supporters: '123'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
