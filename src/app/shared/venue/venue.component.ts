import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-venue',
  templateUrl: './venue.component.html',
  styleUrls: ['./venue.component.scss']
})
export class VenueComponent implements OnInit {
  venue: Array<any> = [
    {
      label: 'Address',
      value: 'Calle 20 55B, Chicxulub, Chicxulub Puerto, Yucatán, Mexico'
    },
    {
      label: 'Landmark',
      value: 'Opposite of JBN Mall'
    },
    {
      label: 'Direction',
      value: 'Get down at the Gateway of India and walk 100 m towards the Mini forest park.'
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
