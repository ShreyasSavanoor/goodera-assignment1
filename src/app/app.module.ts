import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { CarouselComponent } from './shared/carousel/carousel.component';
import { ViewDetailsComponent } from './shared/view-details/view-details.component';
import { OpportunitesComponent } from './shared/opportunites/opportunites.component';
import { DonateComponent } from './shared/donate/donate.component';
import { ButtonComponent } from './shared/form-elements/button/button.component';
import { VenueComponent } from './shared/venue/venue.component';
import { ParticipantsComponent } from './shared/participants/participants.component';
import { MapComponent } from './visuals/map/map.component';
import { EventsComponent } from './shared/events/events.component';
import { DetailsComponent } from './shared/details/details.component';
import { ListsComponent } from './shared/form-elements/lists/lists.component';
import { FooterComponent } from './shared/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CarouselComponent,
    ViewDetailsComponent,
    OpportunitesComponent,
    DonateComponent,
    ButtonComponent,
    VenueComponent,
    ParticipantsComponent,
    MapComponent,
    EventsComponent,
    DetailsComponent,
    ListsComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
