import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'assignment1';
  mainSection = [
    {
      carousel: true,
      imgSrc: 'screen-shot-2019-02-18-at-11-00-25-am'
    },
    {
      carousel: false,
      title: 'Be a food collection agent for a day',
      foundation: 'By Smile foundation',
      checkImg: '',
      date: 'Sunday, October 13, 2019',
      time: '12:00 AM – 3:00 AM (3 hours)',
      venue: 'Chicxulub Puerto, Mexico',
      commitment: '3',
      lastdate: 'October 10, 2019',
      buttonText: 'Volunteer Now'
    }
  ];
  sections: Array<any> = [
    {
      title: 'What you will be doing',
      type: 'events',
      content: 'You will be going to all our Chicxulub Food Bank associate vendors and collect food donations from them. We have over 10 such vendors across the market. The entire process of going around collecting food on our food cart is fun and is sure to make you feel special. And yes, be ready to say ‘Excuse Me’ while moving through the crowd',
      noBackground: false
    },
    {
      title: '134 of your colleagues will be making a difference, join them',
      type: 'participants',
      noBackground: true
    },
    {
      title: 'Where is the volunteering venue?',
      type: 'venue',
      noBackground: false
    },
    {
      title: 'Donate. Impact. Inspire',
      type: 'donate',
      noBackground: true
    },
    {
      title: 'More opportunities. More impact',
      type: 'opportunities',
      noBackground: false
    }
  ];
}
